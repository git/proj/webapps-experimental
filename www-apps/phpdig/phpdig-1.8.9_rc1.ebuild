# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=78388
# Submitted-By: Daniel Webert
# Reviewed-By: rl03 2005-12-11

inherit webapp depend.php

MY_PV=${PV/_/-}
DESCRIPTION="A web spider and search engine written in PHP, using a
MySQL database and flat file support"
HOMEPAGE="http://www.phpdig.net"
SRC_URI="http://www.phpdig.net/scripts/${PN}-${MY_PV}.zip"

LICENSE="GPL-2"
KEYWORDS="~x86"
S=${WORKDIR}/${PN}-${MY_PV}

IUSE="mysql"

DEPEND="app-arch/unzip"
RDEPEND="virtual/httpd-cgi"

need_php_httpd

src_unpack() {
	unpack ${A}
	cd "${S}"
	rm -f .htaccess
	cp "${FILESDIR}"/${PN}.htaccess .htaccess
}

pkg_setup() {
	local flags="unicode"
	use mysql && flags="${flags} mysql"
	require_php_with_use ${flags}
	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst

	dodoc UPGRADE.txt documentation/*.txt information/{CHANGELOG,CREDITS,README}.txt
	dohtml documentation/*.html
	rm -rf UPGRADE.txt documentation/ information/

	cp -R . "${D}"/${MY_HTDOCSDIR}

	local files="admin/temp includes text_content"
	for file in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}/${file}"
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_configfile ${MY_HTDOCSDIR}/includes/config.php

	webapp_src_install
}
