# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="An easy to use, multilingual, flexible photo/image archive/album/gallery written in PHP."
HOMEPAGE="http://linpha.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="GPL-2 || ( BSD LGPL-2.1 )"
KEYWORDS="~amd64 ~x86"
IUSE="imagemagick"

RDEPEND="virtual/httpd-cgi
	imagemagick? ( media-gfx/imagemagick )"

need_php_httpd

pkg_setup() {
	webapp_pkg_setup

	local dbflags="mysql mysqli postgres sqlite"
	if ! PHPCHECKNODIE="yes" require_php_with_any_use ${dbflags} || \
		( ! use imagemagick && ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ) ; then
			eerror
			eerror "${PN} requires PHP with at least one of ${dbflags} USE flags enabled"
			! use imagemagick && eerror "and either gd or gd-external USE flag enabled"
			eerror
			die "Re-install ${PHP_PKG}."
	fi
}

src_install() {
	webapp_src_preinst

	dodoc ChangeLog
	rm -f ChangeLog README
	dohtml docs/*

	cp -R * "${D}"${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/include/db_connect.php
	webapp_serverowned ${MY_HTDOCSDIR}/albums
	webapp_serverowned ${MY_HTDOCSDIR}/sql

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}

pkg_postinst() {
	elog "Using some sort of PHP optimizer is highly recommended for ${PN}."
	elog "If you have not installed one yet, emerge one of the following ebuilds:"
	elog "\t dev-php[45]/eaccelerator"
	elog "\t dev-php[45]/pecl-apc"
	elog "\t dev-php[45]/ZendOptimizer"
	elog

	webapp_pkg_postinst
}
