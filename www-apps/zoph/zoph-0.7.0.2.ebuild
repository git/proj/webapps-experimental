# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=44595
# Submitted-By: Jeroen Roos
# Reviewed-By: rl03 2005-12-18

inherit webapp depend.php

DESCRIPTION="Web based digital image presentation and management system"
HOMEPAGE="http://zoph.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

SLOT="0"
WEBAPP_MANUAL_SLOT="yes"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE="examples"

RDEPEND=">=dev-lang/perl-5.8.8
	>=dev-lang/php-5.2.2
	dev-perl/DBI
	dev-perl/DBD-mysql
	dev-perl/File-Copy-Link
	dev-perl/ImageSize
	>=media-gfx/jhead-1.2
	>=media-gfx/imagemagick-5.3
	virtual/httpd-cgi"

pkg_setup() {
	local flags="exif mysql pcre session xml"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} || \
		! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
			die "Re-install ${PHP_PKG} with ${flags} and either gd or gd-external USE flags enabled."
	fi

	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst

	dodoc [[:upper:]]* sql/* bin/zophrc.sample
	if use examples ; then
		docinto examples
		dodoc contrib/*
	fi
	dohtml docs/*
	doman man/*

	rm -f bin/zophrc.sample
	dobin bin/*

	cp -R php/* "${D}"/${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/config.inc.php
	webapp_sqlscript mysql sql/zoph.sql
	webapp_sqlscript mysql sql/zoph_update-0.7.sql 0.6
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
