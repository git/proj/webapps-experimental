# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Parse a cron entry and check against current time"
HOMEPAGE="http://search.cpan.org/~bits/${P}/"
SRC_URI="mirror://cpan/authors/id/B/BI/BITS/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

SRC_TEST="do"
