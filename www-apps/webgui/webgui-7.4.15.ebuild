# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp eutils

DESCRIPTION="WebGUI is one of the most popular open source content management systems"
HOMEPAGE="http://www.plainblack.com/webgui"
SRC_URI="mirror://sourceforge/pb${PN}/${PN}-${PV/_/-}-stable.tar.gz"

KEYWORDS="~x86"
LICENSE="GPL-2"
IUSE="spell"
RESTRICT="test"

DEPEND=">=dev-lang/perl-5.8.8
	>=dev-perl/Archive-Tar-1.05
	>=dev-perl/Archive-Zip-1.16
	>=dev-perl/Class-InsideOut-1.06
	dev-perl/Color-Calc
	>=dev-perl/Compress-Zlib-1.34
	>=dev-perl/Config-JSON-1.1.0
	>=dev-perl/Data-Structure-Util-0.11
	>=dev-perl/DateTime-0.30
	>=dev-perl/DateTime-Cron-Simple-0.2
	>=dev-perl/DateTime-Format-Mail-0.30
	>=dev-perl/DateTime-Format-Strptime-1.0601
	>=dev-perl/DBD-mysql-3.002
	>=dev-perl/DBI-1.40
	>=dev-perl/Finance-Quote-1.08
	>=dev-perl/HTML-Highlight-0.20
	>=dev-perl/HTML-Parser-3.36
	>=dev-perl/HTML-TagCloud-0.34
	>=dev-perl/HTML-TagFilter-0.07
	>=dev-perl/HTML-Template-2.9
	>=dev-perl/HTML-Template-Expr-0.05
	>=dev-perl/IO-Zlib-1.01
	>=dev-perl/JSON-0.991
	>=dev-perl/libwww-perl-5.800
	dev-perl/Locale-US
	>=dev-perl/Log-Log4perl-0.51
	>=dev-perl/MIME-tools-5.419
	>=dev-perl/Net-Subnets-0.21
	>=dev-perl/perl-ldap-0.25
	>=dev-perl/ParsePlainConfig-1.1
	>=dev-perl/perl-ldap-0.25
	>=dev-perl/Pod-Coverage-0.17
	>=dev-perl/POE-0.32.02
	>=dev-perl/SOAP-Lite-0.60
	>=dev-perl/Test-Deep-0.095
	>=dev-perl/Test-MockObject-1.02
	spell? ( dev-perl/Text-Aspell )
	>=dev-perl/Text-CSV_XS-0.26
	>=dev-perl/Tie-CPHash-1.001
	>=dev-perl/Tie-IxHash-1.21
	>=dev-perl/URI-1.35
	>=dev-perl/XML-RSSLite-0.11
	>=dev-perl/XML-Simple-2.09
	>=dev-perl/Weather-Com-0.5.1
	>=virtual/perl-Digest-MD5-2.20
	>=virtual/perl-Test-Simple-0.61
	>=virtual/perl-Text-Balanced-1.95
	>=virtual/perl-Time-HiRes-1.38
	>=virtual/perl-libnet-1.21
	>=www-apache/libapreq2-2.06
	>=media-gfx/imagemagick-6.0
	=www-apache/mod_perl-2*"

RDEPEND="${DEPEND}
	>=virtual/mysql-5.0
	=www-servers/apache-2*"

S=${WORKDIR}/WebGUI

pkg_setup() {
	webapp_pkg_setup
	if ! built_with_use media-gfx/imagemagick perl; then
		die "Please recompile media-gfx/imagemagick with perl support"
	fi
}

src_install() {
	webapp_src_preinst

	dodoc docs/*.txt docs/changelog/*.txt
	cp -R . "${D}"/${MY_HTDOCSDIR}

	webapp_serverowned ${MY_HTDOCSDIR}/www/uploads
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_hook_script "${FILESDIR}"/reconfig
	webapp_src_install
}

src_test() {
	cd "${S}"/sbin
	/usr/bin/perl testEnvironment.pl --simpleReport > "${T}"/t
	if ! grep -q "Testing complete!" "${T}"/t; then
		ewarn "Missing Perl dependency!"
		ewarn
		cat "${T}"/t
		ewarn
		ewarn "Please file a ticket in our trac"
		die "Missing dependencies."
	fi
}
