0. PHP CONFIGURATION

When operating Serendipity in PHP's Safe Mode, you will not be able to use 
File Upload and SPARTACUS Plugin fetching facilities. 

Enabling error_reporting and display_errors (or directing this to a logfile) 
is suggested to keep track of possible errors. 

The file_uploads directory should be turned ON if you want to have media file 
upload features. 

It is very much suggested to DISABLE the register_globals, session.use_trans_sid
and magic_quotes_* directives of PHP if you have not done so yet.

1. SETUP

Now that you've installed this, you can call the installer interface via
http://${VHOST_HOSTNAME}${VHOST_APPDIR}/index.php and follow the directions.

See http://www.s9y.org/36.html#A6 if you need more help.
