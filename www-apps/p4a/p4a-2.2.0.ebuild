# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=29886
# Reviewed-By: rl03 2005-12-16

inherit webapp depend.apache depend.php

DESCRIPTION="PHP framework containing libraries, modules and widgets
used to build applications that will be used with a web browser connected to the net."
HOMEPAGE="http://p4a.crealabsfoundation.org/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2 LGPL-2.1"
KEYWORDS="~x86"
IUSE="examples"

RDEPEND="dev-php/PEAR-HTML_Template_Flexy"

need_php_httpd
need_apache

pkg_setup() {
	require_php_with_any_use db2 firebird mssql mysql mysqli oci8 odbc postgres sapdb sqlite sybase
	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst

	dodoc CHANGELOG README
	dohtml -r docs/*
	rm -rf CHANGELOG COPYING README docs/

	use examples || rm -rf applications

	cp -R . "${D}"/${MY_HTDOCSDIR}

	webapp_src_install
}
