# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=66654
# Submitted-By: Alexandru Toma
# Reviewed-By: rl03 2005-12-13

inherit webapp depend.apache depend.php

DESCRIPTION="A program to grab and save comic strips from the web"
HOMEPAGE="http://phpgrabcomics.org/server"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

need_php_httpd
need_apache

S=${WORKDIR}

src_unpack() {
	unpack ${A}
	cd "${S}"
	find . -type d -name 'CVS' -print | xargs rm -rf
}

pkg_setup() {
	if ! PHPCHECKNODIE="yes" require_php_with_use mysql || \
		! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
			die "Re-install ${PHP_PKG} with mysql and either gd or gd-external in USE."
	fi
	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	dodoc AUTHOR CHANGELOG TODO

	cp -R www/* "${D}"/${MY_HTDOCSDIR}

	local files="inc/config.php comics tmp"
	for file in ${files}; do
		webapp_serverowned ${MY_HTDOCSDIR}/${file}
	done

	webapp_configfile ${MY_HTDOCSDIR}/inc/config.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
