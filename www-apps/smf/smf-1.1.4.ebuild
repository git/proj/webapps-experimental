# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp versionator depend.php

MY_PV=$(replace_all_version_separators '-')
DESCRIPTION="Simple Machines Forum is an elegant, powerful, easy, and free community forum software package."
HOMEPAGE="http://www.simplemachines.org/"
SRC_URI="http://www.simplemachines.org/download.php/index.php/${PN}_${MY_PV}_install.tar.bz2"

LICENSE="simplemachines"
KEYWORDS="~amd64 ~x86"
RESTRICT="fetch"
IUSE="gd spell"

DEPEND=""
RDEPEND="virtual/httpd-cgi
	spell? ( app-text/aspell )"

need_php_httpd

S=${WORKDIR}

pkg_nofetch() {
	einfo "Please download ${PN}_${MY_PV}_install.tar.bz2 from:"
	einfo "http://www.simplemachines.org/download/index.php"
	einfo "and move it to ${DISTDIR}"
}

pkg_setup() {
	local flags="mysql"
	use spell && flags="${flags} spell"
	use gd && flags="${flags} and either gd or gd-external"
	if ! PHPCHECKNODIE="yes" require_php_with_use mysql || \
		( use spell && ! PHPCHECKNODIE="yes" require_php_with_use spell ) || \
			( use gd && ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ) ; then
				die "Re-install ${PHP_PKG} with ${flags} in USE."
	fi
	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst
	local files="attachments avatars Packages Packages/installed.list Smileys
		Themes agreement.txt Settings_bak.php Settings.php"

	dodoc changelog.txt
	dohtml news_readme.html readme.html
	rm -f {changelog,license}.txt

	cp -R . "${D}"/${MY_HTDOCSDIR}

	for a in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}/${a}"
	done

	webapp_configfile ${MY_HTDOCSDIR}/Settings.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}

pkg_postinst() {
	ewarn
	ewarn "Please do NOT try to upgrade ${PN} using \"webapp-config -U\""
	ewarn "SMF provides it's separate upgrade package and you will find"
	ewarn "the instructions at http://docs.simplemachines.org/index.php?board=3.0"
	ewarn
	webapp_pkg_postinst
}
