# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=28376
# Submitted-By: Nathan Horter
# Reviewed-By: rl03 2005-12-09

inherit webapp depend.php

DESCRIPTION="Geeklog is a weblog powered by PHP and MySQL"
HOMEPAGE="http://www.geeklog.net"
SRC_URI="http://www.emanuele-gentili.com/gentoo/geeklog/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php_httpd

pkg_setup() {
	require_php_with_use mysql
	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst

	dodoc readme
	rm -f readme INSTALL
	dohtml -r public_html/docs/*

	dodir ${MY_HOSTROOTDIR}/${PF}
	cp -R public_html/* "${D}"/${MY_HTDOCSDIR}
	cp -R [^p]* plugins "${D}"/${MY_HOSTROOTDIR}/${PF}

	local files="backend backend/geeklog.rss images/articles images/topics	images/userphotos"
	for file in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}/${file}"
	done

	local files2="logs logs/error.log logs/access.log data"
	for file in ${files2}; do
		webapp_serverowned "${MY_HOSTROOTDIR}/${PF}/${file}"
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_hook_script "${FILESDIR}"/reconfig

	webapp_src_install
}
