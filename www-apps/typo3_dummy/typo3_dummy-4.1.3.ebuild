# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp

MY_PN="dummy"
DESCRIPTION="TYPO3 is a free Open Source CMS. This is the dummy package."
HOMEPAGE="http://www.typo3.com/  http://typo3.org/"
SRC_URI="mirror://sourceforge/typo3/${MY_PN}-${PV}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

PDEPEND="~www-apps/typo3_src-${PV}"

S=${WORKDIR}/${MY_PN}-${PV}

src_unpack() {
	unpack ${A}
	cd "${S}"
	rm -f typo3_src
}

src_install() {
	webapp_src_preinst
	dodoc *.txt

	cp -R . "${D}"/${MY_HTDOCSDIR}

	local files="fileadmin typo3conf typo3temp uploads typo3conf/localconf.php"

	for file in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}/${file}"
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_configfile ${MY_HTDOCSDIR}/typo3conf/localconf.php
	webapp_hook_script "${FILESDIR}"/reconfig
	webapp_src_install
}
