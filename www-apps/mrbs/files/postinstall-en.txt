You are almost done!

  0. Create a new database:
	mysqladmin create mrbs , or
	createdb mrbs

	Then create tables:
	mysql mrbs < ${VHOST_ROOT}/${PN}-${PVR}/tables.my.sql , or
	psql -a -f < ${VHOST_ROOT}/${PN}-${PVR}/tables.pg.sql mrbs
	
	You may need to set rights on the tables; for PostgreSQL see "grant.pg.sql".
	If you use a PHP version less than 4.2, you will also need to replace the
	pgsql driver file pgsql.inc with pgsql.before_php42.inc, and rename it to
	pgsql.inc.
	If you need to delete the tables, for PostgreSQL see "destroy.pg.sql".

	If you want to add sample data, load in sample-data.sql

	If you are upgrading, use upgrade.my.sql

  1. Edit ${MY_INSTALLDIR}/config.inc.php

  2. Consult /usr/share/doc/${PN}-${PVR}/AUTHENTICATION.gz for authentication
  support.

  3. See /usr/share/doc/${PN}-${PVR}/README.gz for login instructions.
