# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit webapp eutils

DESCRIPTION="Legend of the Green Dragon is a remake of the old BBS game Legend of the Red Dragon (aka LoRD)."
HOMEPAGE="http://www.lotgd.net/"
SRC_URI="mirror://sourceforge/lotgd/${P}.tar.gz"

LICENSE="CCPL-Attribution-ShareAlike-2.0"
KEYWORDS="~x86"

RDEPEND="virtual/php
	dev-db/mysql"

src_unpack() {
	unpack ${A}
	# http://dragonprime.net/index.php?board=5;action=display;threadid=2118#msg19756
	epatch ${FILESDIR}/${P}-fix-changing-master-bug.diff
	# http://dragonprime.net/index.php?board=5;action=display;threadid=2118#msg19753
	epatch ${FILESDIR}/${P}-fix-commentary-bug.diff
	# http://dragonprime.net/index.php?board=21;action=display;threadid=2154#msg20374
	epatch ${FILESDIR}/${P}-fix-game-settings-bug.diff
	# http://dragonprime.net/index.php?board=16;action=display;threadid=2238#msg21156
	epatch ${FILESDIR}/${P}-fix-translating-bug.diff
}

src_install() {
	webapp_src_preinst

	dodoc LICENSE.txt README.txt TODO.txt
	rm -f README.txt TODO.txt

	cp -a * "${D}/${MY_HTDOCSDIR}"

	webapp_src_install
}
