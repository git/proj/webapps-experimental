# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Runtime require of both modules and files."
SRC_URI="mirror://cpan//authors/id/K/KA/KANE/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~kane/${P}"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""

SRC_TEST="do"
