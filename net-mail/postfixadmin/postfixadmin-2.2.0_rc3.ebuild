# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils webapp depend.php confutils

DESCRIPTION="Web Based Management tool for Postfix for dealing with Postfix Style Virtual Domains
and Virtual Users that are stored in MySQL/PostgreSQL."
HOMEPAGE="http://sourceforge.net/projects/postfixadmin/"
SRC_URI="mirror://sourceforge/${PN}/${P/_/-}.tar.gz"

LICENSE="MPL-1.1"
KEYWORDS="~x86 ~amd64"
IUSE="extras mysql postgres vacation"

RDEPEND=">=mail-mta/postfix-2.0.0
	!www-apps/postfixadmin
	vacation?  ( dev-perl/DBI
			dev-perl/Email-Valid
			dev-perl/Mail-Sendmail
			mysql? ( dev-perl/DBD-mysql )
			postgres? ( dev-perl/DBD-Pg )
		    )"

need_php_httpd
need_httpd_cgi

S="${WORKDIR}/${P/_/-}"

pkg_setup() {
	webapp_pkg_setup

	confutils_require_any mysql postgres
	confutils_use_depend_built_with_all mysql mail-mta/postfix mysql
	confutils_use_depend_built_with_all postgres mail-mta/postfix postgres

	local flags="pcre session"
	use postgres && flags="${flags} postgres"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} || \
		( use mysql && ! PHPCHECKNODIE="yes" require_php_with_any_use mysql mysqli ) ; then
			local diemsg="Re-install ${PHP_PKG} with ${flags}"
			use mysql && diemsg="${diemsg} and at least one of ${dbflags}"
			die "${diemsg} USE flags enabled."
	fi

	if use vacation ; then
		enewgroup vacation
		enewuser vacation -1 -1 -1 vacation
	fi
}

src_unpack() {
	unpack ${A}
	cd "${S}"

	# remove useless cruft
	rm -rf debian/
	rm -f something.tar.gz

	use mysql && mv DATABASE_MYSQL.TXT "${T}"/mysql-setup.sql
	if use postgres ; then
		mv DATABASE_PGSQL.TXT "${T}"/pgsql-setup.sql
		# TODO: these patches need checking/update
		# epatch "${FILESDIR}"/${P}-postgres-1.patch || die "Patching for postgres failed."
		# epatch "${FILESDIR}"/${P}-postgres-2.patch || die "Patching for postgres failed."
		# epatch "${FILESDIR}"/${P}-postgres-3.patch || die "Patching for postgres failed."
	fi
}

src_install() {
	webapp_src_preinst

	if use vacation ; then
		diropts -m0770 -o vacation -g vacation
		keepdir /var/spool/vacation
		insinto /var/spool/vacation
		insopts -m770 -o vacation -g vacation
		doins "${S}"/VIRTUAL_VACATION/vacation.pl
	fi


	# handle documentation files
	local docs="CHANGELOG.TXT INSTALL.TXT"
	dodoc ${docs} DOCUMENTS/*.txt

	if use vacation ; then
		newdoc VIRTUAL_VACATION/INSTALL.TXT VACATION_INSTALL.TXT
		newdoc VIRTUAL_VACATION/FILTER_README VACATION_FILTER_README
	fi

	if use extras ; then
		docinto extras
		dodoc ADDITIONS/*.{php,pl,sh,tgz} theme-support.patch
		newdoc ADDITIONS/README.TXT README_EXTRAS.TXT
		insinto /usr/share/doc/${PF}/screenshots
		doins DOCUMENTS/screenshots/*.jpg
	fi
	rm -rf ${docs} GPL-LICENSE.TXT LICENSE.TXT theme-support.patch ADDITIONS/ DOCUMENTS/ VIRTUAL_VACATION/

	insinto ${MY_HTDOCSDIR}
	doins -r .

	if use mysql; then
		webapp_sqlscript mysql "${T}"/mysql-setup.sql
		webapp_postinst_txt en "${FILESDIR}"/postinstall-en-mysql.txt
	fi
	if use postgres; then
		webapp_sqlscript postgresql "${T}"/postgres-setup.sql
		webapp_postinst_txt en "${FILESDIR}"/postinstall-en-postgres.txt
	fi

	webapp_configfile ${MY_HTDOCSDIR}/config.inc.php

	webapp_src_install
}
