# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Support for creating standard inside-out classes."
HOMEPAGE="http://search.cpan.org/~dconway/${P}"
SRC_URI="mirror://cpan/authors/id/D/DC/DCONWAY/${PN}-v${PV}.tar.gz"
IUSE=""

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-perl/version
	virtual/perl-Scalar-List-Utils"

S="${WORKDIR}/${PN}-v${PV}"
