# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp versionator depend.php

DESCRIPTION="Web based (PHP Script) Jukebox"
HOMEPAGE="http://www.mp3act.net/"
MY_PV=$(replace_version_separator 1 '')
SRC_URI="mirror://sourceforge/${PN}/${PN}_${MY_PV}.tar.bz2"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE="mysql"

S=${WORKDIR}

DEPEND="media-sound/lame
	virtual/httpd-cgi
	virtual/mpg123"

need_php_httpd

pkg_setup() {
	local flags="cli session"
	use mysql && flags="${flags} mysql"
	require_php_with_use ${flags}

	webapp_pkg_setup
}

src_unpack() {
	unpack ${A}
	cd "${S}"
	cp includes/mp3act_config.dist.php includes/mp3act_config.php
}

src_install() {
	webapp_src_preinst

	dodoc docs/changes.txt docs/install.txt
	rm -rf docs/

	cp -R * "${D}"/${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/includes/mp3act_config.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
