# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit flag-o-matic eutils fixheadtails gnuconfig

DESCRIPTION="Classic Forum"
HOMEPAGE="http://wwwtech.de/cforum/"
SRC_URI="http://wwwtech.de/cforum/download/${P}.tgz"

LICENSE="Artistic"
SLOT="0"
KEYWORDS="~x86"
IUSE="debug mailonpost mysql shared-mem sorting"

DEPEND="sys-libs/db
	dev-lang/perl
	sys-libs/zlib
	dev-libs/gdome2
	dev-libs/openssl
	mysql? ( virtual/mysql )
	dev-libs/libpcre
	dev-lang/swig
	net-misc/curl
	mailonpost? ( net-libs/libesmtp )"

src_compile() {
	local myconf disabled

	if ! use mailonpost ; then
		disabled="flt_mailonpost.la"
	fi

	if ! use mysql ; then
		if ! useq mailonpost; then
			disabled="flt_admin_mysql.la"
		else
			disabled="${disabled} flt_admin_mysql.la"
		fi
	fi

	if [[ -n "${disabled}" ]] ; then
		myconf="${myconf} --with-disable-modules='${disabled}'"
	fi

	myconf="--prefix=/usr \
		--datadir=/usr/share \
		--sysconfdir=/etc \
		--localstatedir=/var/lib \
		$(use_enable debug) \
		$(use_enable shared-mem) \
		$(use_enable sorting)"

	econf ${myconf}
	emake || die "problem compiling cforum :("
}

src_install () {
	emake -j1 DESTDIR="${D}" install || die
	dodoc AUTHORS ChangeLog THANKS TODO
}

pkg_postinst() {
	elog
	elog "You now have to configure the Classic Forum. Follow the instructions"
	elog "of the global installation tutorial at"
	elog "   <http://wwwtech.de/cforum/wiki/moin.cgi/Tutorials>."
	elog "Config files have been placed in /etc/cforum/."
	elog
}
