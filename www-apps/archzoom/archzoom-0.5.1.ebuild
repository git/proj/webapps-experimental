# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=103155
# Submitted-By: Matteo Settenvini
# Reviewed-By: rl03 2005-12-19

inherit webapp

DESCRIPTION="Archzoom is a web interface for GNU Arch (tla) 1.x archives"
HOMEPAGE="http://migo.sixbit.org/software/archzoom/"
SRC_URI="http://migo.sixbit.org/software/${PN}/releases/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

RDEPEND="dev-util/tla
	>=dev-lang/perl-5.1
	>=app-text/enscript-1.6
	virtual/httpd-cgi"

src_install () {
	webapp_src_preinst

	dodoc AUTHORS ChangeLog NEWS README TODO doc/*

	dodir ${MY_HOSTROOTDIR}/${PF} ${MY_CGIBINDIR}/${PN}
	exeinto ${MY_CGIBINDIR}/${PN}
	doexe bin/archzoom.cgi

	local datadir="${MY_HOSTROOTDIR}/${PF}/archzoom-data"
	dodir ${datadir}/var/cache
	keepdir ${datadir}/var/cache

	find perllib -name "Makefile*" | xargs rm
	find perllib -name "*pm.in" | xargs rm

	cp -r perllib templates "${D}"/${datadir}/

	insinto ${datadir}/conf
	doins conf/archzoom.conf

	webapp_configfile ${datadir}/conf/archzoom.conf
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_hook_script "${FILESDIR}"/reconfig

	webapp_src_install
}
