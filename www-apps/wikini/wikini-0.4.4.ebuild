# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=88376
# Submitted-By: didibaba
# Reviewed-By: rl03 2005-12-16

inherit webapp depend.php

DESCRIPTION="Yet another wiki written in PHP."
HOMEPAGE="http://www.wikini.net"
SRC_URI="http://www.wikini.net/download/${P}.tar.gz"

LICENSE="GPL-2 BSD"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php_httpd

S=${WORKDIR}/${PN}

pkg_setup() {
	require_php_with_use mysql
	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst

	local docs="INSTALL README"
	dodoc ${docs}
	rm -f ${docs} COPYING LICENSE

	cp -R . "${D}"/${MY_HTDOCSDIR}

	touch ${MY_HTDOCSDIR}/wakka.config.php
	webapp_serverowned ${MY_HTDOCSDIR}/wakka.config.php

	webapp_src_install
}
