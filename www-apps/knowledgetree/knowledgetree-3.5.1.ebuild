# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

MY_PN=ktdms-src-oss
MY_PV=${PV/_/}
MY_P=${MY_PN}-${MY_PV}

DESCRIPTION="KnowledgeTree is a document management system providing a content repository, workflow and routing of content, content publication and content metrics definition and analysis."
HOMEPAGE="http://www.knowledgetree.com/"
SRC_URI="mirror://sourceforge/kt-dms/${MY_P}.tgz"

LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"
IUSE="openoffice catdoc"

# Need to make these all depend on IUSE flags?
# The indexers and unzip are really needed for normal functioning.
# catdoc is masked so make it optional for now.
# curl is required for various tasks.
# openoffice is optional but recommended - for PDF generation, document comparison, etc.
RDEPEND="virtual/httpd-cgi
		 dev-php/smarty
		 dev-php/PEAR-Cache_Lite
		 dev-php/PEAR-Config
		 dev-php/PEAR-PHP_CompatInfo
		 dev-php/PEAR-DB
		 dev-php/PEAR-File
		 dev-php/PEAR-File_Gettext
		 dev-php/PEAR-HTTP
		 dev-php/PEAR-Log
		 dev-php/PEAR-Net_URL
		 >=app-text/catdoc-0.94.1
		 app-text/pstotext
		 app-text/poppler
		 app-arch/unzip
		 net-misc/curl
		 ssl? ( dev-libs/openssl )
		 openoffice? ( || (  app-office/openoffice-bin app-office/openoffice ) )"

# PHP5 is now required.
need_php5_httpd

# How do we handle required PHP modules if needed?

S="${WORKDIR}"/knowledgeTree-OSS

pkg_setup() {
	require_php_with_use "session nls json mysqli mysql"

	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	## Main application
	cp -r . "${D}${MY_HTDOCSDIR}"

	dodoc HISTORY.txt docs/* examples

	## Docs installed, remove unnecessary files
	rm "${D}${MY_HTDOCSDIR}"/HISTORY.txt
	rm "${D}${MY_HTDOCSDIR}"/docs/README.txt
	rm "${D}${MY_HTDOCSDIR}"/docs/CHANGELOG*
	rm -rf "${D}${MY_HTDOCSDIR}"/examples

	# we remove the third-party tools/softwares bundled with KTDMS
	# (instead we use the ones provided by the portage tree)
	rm -rf "${D}${MY_HTDOCSDIR}"/thirdparty/Smarty/
	rm -rf "${D}${MY_HTDOCSDIR}"/thirdparty/pear/

	for CFG in "${MY_HTDOCSDIR}"/config/*
	do
	  webapp_configfile "${CFG}"
	done

	keepdir "${MY_HTDOCSDIR}"/var/cache

	## Documents will be saved here
	webapp_serverowned "${MY_HTDOCSDIR}"/var/cache
	webapp_serverowned "${MY_HTDOCSDIR}"/var/Documents
	webapp_serverowned "${MY_HTDOCSDIR}"/var/indexes
	webapp_serverowned "${MY_HTDOCSDIR}"/var/log
	webapp_serverowned "${MY_HTDOCSDIR}"/var/tmp
	webapp_serverowned "${MY_HTDOCSDIR}"/var/uploads

	webapp_sqlscript mysql    sql/mysql/install/structure.sql
	webapp_sqlscript mysql    sql/mysql/install/data.sql

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_postupgrade_txt en "${FILESDIR}"/postupgrade-en.txt

	## Fix for the root folder
	webapp_hook_script "${FILESDIR}"/config-hook.sh

	webapp_src_install
}
