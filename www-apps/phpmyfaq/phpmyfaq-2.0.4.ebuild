# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="open source FAQ system for PHP"
HOMEPAGE="http://www.phpmyfaq.de"
SRC_URI="http://www.phpmyfaq.de/download/${P}.tar.gz"

LICENSE="MPL-1.1"
KEYWORDS="~x86"
IUSE="captcha ldap"

RDEPEND="virtual/httpd-cgi"
need_php_httpd

pkg_setup() {
	webapp_pkg_setup

	local dbflags="db2 firebird interbase mssql mysql mysqli oci8 postgres sqlite sybase"
	if ! PHPCHECKNODIE="yes" require_php_with_any_use ${dbflags} || \
		( use captcha && ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ) || \
			( use ldap && ! PHPCHECKNODIE="yes" require_php_with_use ldap ) ; then
				eerror
				eerror "${PHP_PKG} needs to be re-installed with at least one of these"
				eerror "USE flags enabled: ${dbflags}"
				eerror
				use captcha && eerror "You also either gd or gd-external USE flag enabled."
				use ldap && eerror "You also need ldap USE flag enabled."
				die "Re-install ${PHP_PKG}"
	fi
}

src_install() {
	webapp_src_preinst

	dodoc docs/*.txt
	for i in _* ; do
		newdoc ${i/_/}
		rm -f ${i}
	done
	dohtml docs/*.html
	rm -rf docs/*.txt ${PN}.spec scripts/

	cp inc/data.php.original inc/data.php
	touch inc/dataldap.php
	cp -R . "${D}"${MY_HTDOCSDIR}

	webapp_serverowned -R ${MY_HTDOCSDIR}/install
	webapp_serverowned ${MY_HTDOCSDIR}/inc
	webapp_serverowned ${MY_HTDOCSDIR}/inc/data.php
	webapp_serverowned ${MY_HTDOCSDIR}/inc/dataldap.php

	webapp_configfile ${MY_HTDOCSDIR}/inc/data.php
	webapp_configfile ${MY_HTDOCSDIR}/inc/dataldap.php

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
