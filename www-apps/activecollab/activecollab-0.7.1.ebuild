# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit depend.php webapp

DESCRIPTION="activeCollab is a free alternative to 37 Signals' Basecamp"
HOMEPAGE="http://www.activecollab.com/"
SRC_URI="http://www.activecollab.com/files/0.7.1/activeCollab.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php5_httpd

S="${WORKDIR}/${PN}"

pkg_setup() {
	require_php_with_use mysql
	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	dodoc readme.txt
	rm -f readme.txt license.txt

	cp -r * "${D}"/${MY_HTDOCSDIR}

	for x in cache config public upload ; do
		webapp_serverowned -R ${MY_HTDOCSDIR}/${i}
	done

	webapp_configfile ${MY_HTDOCSDIR}/config/config.php

	webapp_src_install

	for x in public/files/avatars public/files/logos public/files/project_documents cache upload ; do
		keepdir ${MY_HTDOCSDIR}/${x}
	done
}
