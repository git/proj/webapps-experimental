# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

MY_P="${PN}-v${PV}"
DESCRIPTION="Load and save configuration files in a standard format."
HOMEPAGE="http://search.cpan.org/~dconway/${MY_P}"
SRC_URI="mirror://cpan/authors/id/D/DC/DCONWAY/${MY_P}.tar.gz"
IUSE=""

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-perl/Class-Std
	dev-perl/version"

S="${WORKDIR}/${MY_P}"
