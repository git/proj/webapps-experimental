# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Computing subnets in large scale networks."
SRC_URI="mirror://cpan/authors/id/S/SR/SRI/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~sri/${P}"
IUSE=""

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"

SRC_TEST="do"
