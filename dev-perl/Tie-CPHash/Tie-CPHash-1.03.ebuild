# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Case preserving but case insensitive hash table"
SRC_URI="mirror://cpan/authors/id/C/CJ/CJM/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~cjm/${P}"
IUSE=""

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"

SRC_TEST="do"

DEPEND="dev-perl/module-build
	virtual/perl-File-Spec"
