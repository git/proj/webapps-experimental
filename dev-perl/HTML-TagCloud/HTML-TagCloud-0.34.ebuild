# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Generate a HTML Tag Cloud"
SRC_URI="mirror://cpan/authors/id/L/LB/LBROCARD/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~lbrocard/${P}"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""

DEPEND="dev-perl/module-build"
RDEPEND=""

SRC_TEST="do"
