# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=90505
# Submitted-By: G. Wrobel
# Reviewed-By: wrobel 2005-12-13

inherit eutils webapp depend.php

MY_PV=${PV//./-}

DESCRIPTION="PHP general-purpose virtual commonplace for customers and developers to collaborate in developing projects."
HOMEPAGE="http://outreach.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/OPT_${MY_PV}_MAX.tar.gz"

LICENSE="GPL-2"
IUSE=""
KEYWORDS="~x86"

RDEPEND="dev-php4/jpgraph
	media-fonts/corefonts
	virtual/httpd-cgi"

need_php4_httpd

S=${WORKDIR}/OPT_${MY_PV}_MAX

pkg_setup() {
	require_php_with_use mysql
	webapp_pkg_setup
}

src_unpack() {
	unpack ${A}
	cd "${S}"/opt

	epatch "${FILESDIR}"/gentoo-jpgraph.patch
	epatch "${FILESDIR}"/gentoo-mysql-4.patch
}

src_install() {
	webapp_src_preinst

	local DOCS="CHANGES INSTALL README docs/{FAQ.txt,INSTALL.email}"
	dodoc ${DOCS}
	rm -rf ${DOCS} docs/

	cp -R opt/* "${D}"/${MY_HTDOCSDIR}

	# Remove bundled php libs
	rm -rf "${D}"/${MY_HTDOCSDIR}/include/jpgraph
	rm -rf "${D}"/${MY_HTDOCSDIR}/include/horde

	# Change permissions for web-based setup
	for srvown in include/setup_lock include/OPT_lib.php include/mail_retrieve.pl include/cvs_parse.pl ; do
		touch "${D}"${MY_HTDOCSDIR}/${srvown}
		webapp_serverowned ${MY_HTDOCSDIR}/${srvown}
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
