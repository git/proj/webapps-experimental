# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Two letter codes for state identification in the United States and vice versa."
SRC_URI="mirror://cpan/authors/id/T/TB/TBONE/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~tbone/${P}"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""

SRC_TEST="do"
