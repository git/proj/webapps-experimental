# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="Web application using PHP and MySQL/PostgreSQL for booking meeting rooms."
HOMEPAGE="http://mrbs.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php_httpd

pkg_setup() {
	require_php_with_any_use mysql mysqli postgres
	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	dodoc [[:upper:]]* web/ChangeLog
	dodir ${MY_HOSTROOTDIR}/${PF}

	cp -R web/* "${D}"/${MY_HTDOCSDIR}
	cp [:a-u:]* "${D}"/${MY_HOSTROOTDIR}/${PF}

	webapp_configfile ${MY_HTDOCSDIR}/config.inc.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
