# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=81276
# Submitted-By: gurligebis
# Reviewed-By: rl03 2005-12-11

inherit versionator webapp depend.php

MY_PV=$(delete_all_version_separators ${PV} )
DESCRIPTION="Simple PHP Blog, a blog storing its data in files"
HOMEPAGE="http://www.simplephpblog.com/"
SRC_URI="mirror://sourceforge/${PN}/${PN}_${MY_PV}.zip"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND="virtual/httpd-cgi"

need_php_httpd

S=${WORKDIR}

src_install () {
	webapp_src_preinst

	dodoc docs/*
	rm -rf docs/

	cp -R . "${D}"/${MY_HTDOCSDIR}

	local files="config content images"
	for x in ${files}; do
		dodir ${MY_HTDOCSDIR}/${x}
		webapp_serverowned "${MY_HTDOCSDIR}/${x}"
	done

	webapp_src_install
}

pkg_postinst() {
	elog "You can download the user manual from:"
	elog "http://sourceforge.net/project/showfiles.php?group_id=106696&package_id=203723&release_id=523419"
	
	webapp_pkg_postinst
}
