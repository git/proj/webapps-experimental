# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="A web client for the Boss Ogg music server."
HOMEPAGE="http://bossogg.wishy.org/"
SRC_URI="mirror://sourceforge/bossogg/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php4_httpd

S=${WORKDIR}/${PN}

src_install() {
	webapp_src_preinst

	docs="README TODO"
	dodoc ${docs}
	rm -f ${docs} COPYING

	cp -R . "${D}"${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/config.php
	webapp_serverowned ${MY_HTDOCSDIR}
	webapp_src_install
}

pkg_postinst() {
	if ! has_version media-sound/bossogg ; then
		ewarn "Unless you want to use ${PN} with a remote server, you need to"
		ewarn "install media-sound/bossogg so that ${PN} is actually useful."
	fi

	webapp_pkg_postinst
}
