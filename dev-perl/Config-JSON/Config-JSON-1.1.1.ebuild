# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="A JSON based config file system."
SRC_URI="http://search.cpan.org/CPAN/authors/id/R/RI/RIZEN/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~makamaka/${P}"

SLOT="0"
LICENSE="|| ( GPL-2 Artistic )"
KEYWORDS="~x86"
IUSE=""

DEPEND="dev-perl/JSON
	dev-perl/Class-InsideOut"

SRC_TEST="do"
