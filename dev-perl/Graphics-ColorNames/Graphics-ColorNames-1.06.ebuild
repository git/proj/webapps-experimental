# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Defines RGB values for common color names."
SRC_URI="mirror://cpan//authors/id/R/RR/RRWO/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~rrwo/${P}"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""

RDEPEND="dev-perl/Module-Load
	dev-perl/Test-Pod"

SRC_TEST="do"
