# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit	webapp depend.php

DESCRIPTION="Open Source forum package"
HOMEPAGE="http://www.deluxebb.com/"
SRC_URI="http://www.deluxebb.com/download/${PN}${PV/./_}.zip"

LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"

DEPEND="app-arch/unzip"
RDEPEND="virtual/httpd-cgi"

need_php_httpd

S="${WORKDIR}"

pkg_setup() {
	require_php_with_use mysql
	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	dodoc docs/{changelog,install,upgrade}.txt

	cp -pPR * "${D}/${MY_HTDOCSDIR}"
	rm -rf "${D}/${MY_HTDOCSDIR}/docs"

	local files="settings/info.php logs/cp.php"
	for file in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}/${file}"
	done

	webapp_configfile "${MY_HTDOCSDIR}/settings/info.php"
	webapp_postinst_txt en "${FILESDIR}/postinstall-en.txt"

	webapp_src_install
}
