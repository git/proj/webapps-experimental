# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="WDG HTML documentation from htmlhelp.com"
HOMEPAGE="http://www.htmlhelp.com"
SRC_URI="http://www.htmlhelp.com/distribution/${PN}.tar.gz"

LICENSE="OPL"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
RESTRICT="strip binchecks"

S=${WORKDIR}/${PN}

src_install() {
	dodir /usr/share/doc/${PF}
	cp -R . "${D}"/usr/share/doc/${PF}
}
