# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=63698
# Submitted-By: egore
# Reviewed-By: rl03 2005-12-14

inherit webapp depend.php depend.apache

DESCRIPTION="BBClone - A PHP based Web Counter on Steroids"
HOMEPAGE="http://www.bbclone.de"
SRC_URI="http://bbclone.de/dist/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php_httpd
want_apache

S="${WORKDIR}/${PN}"

src_install () {
	webapp_src_preinst

	dodoc doc/*
	rm -rf doc/

	cp -R . "${D}"/${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/conf/config.php

	for a in var/* var/.htalock; do
		webapp_serverowned ${MY_HTDOCSDIR}/${a}
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}

pkg_postinst() {
	if use apache2 ; then
		elog "See install.txt in /usr/share/doc/${PF} for hints on using"
		elog "${PN} as a counter without having to modify your webpages."
	else
		ewarn "You are not using apache as webserver, the .htaccess method"
		ewarn "for monitoring your webpages will NOT work!"
	fi

	webapp_pkg_postinst
}
