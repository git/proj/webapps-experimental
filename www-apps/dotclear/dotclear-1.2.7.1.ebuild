# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="Weblogs management tool written in PHP with a MySQL backend"
HOMEPAGE="http://www.dotclear.net"
SRC_URI="http://download.dotclear.net/latest/${P}.tar.gz
	http://dev.gentooexperimental.org/~jakub/distfiles/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

RDEPEND="virtual/httpd-cgi
	virtual/httpd-php"

S=${WORKDIR}/${PN}

pkg_setup() {
	require_php_with_use mysql
	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	local docs="CHANGELOG LISEZMOI.txt VERSION"
	dodoc ${docs}
	rm -f ${docs} COPYING

	cp -r * "${D}"${MY_HTDOCSDIR}

	webapp_serverowned ${MY_HTDOCSDIR}/conf
	webapp_serverowned ${MY_HTDOCSDIR}/conf/UPDATE
	webapp_serverowned ${MY_HTDOCSDIR}/images
	webapp_serverowned ${MY_HTDOCSDIR}/themes
	webapp_serverowned ${MY_HTDOCSDIR}/ecrire/tools
	webapp_serverowned ${MY_HTDOCSDIR}/share

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
