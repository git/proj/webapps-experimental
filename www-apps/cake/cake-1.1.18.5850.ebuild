# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="A rapid development framework for PHP"
HOMEPAGE="http://cakephp.org/"

# Ugly hack until the source is provided w/o going through the donation
# interstitial and to handle the underscore...
MY_P="${PN}_${PV}"
DLID="549"
SRC_URI="http://cakeforge.org/frs/download.php/${DLID}/${MY_P}.tar.bz2/donation=complete/${MY_P}.tar.bz2"

LICENSE="MIT"
KEYWORDS="~amd64 ~x86"
IUSE="adodb"

RDEPEND="virtual/httpd-cgi
	adodb? ( dev-php/adodb )"

need_php_httpd

S=${WORKDIR}/${MY_P}

pkg_setup () {
	webapp_pkg_setup

	local flags="xml session"
	local dbflags="mysql postgres sqlite"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} || \
		( ! use adodb && ! PHPCHECKNODIE="yes" require_php_with_any_use ${dbflags} ) ; then
			die "Re-install ${PHP_PKG} with ${flags} and at least one of ${dbflags} USE flags enabled."
	elif
		use adodb && ! PHPCHECKNODIE="yes" require_php_with_any_use ${dbflags} ; then
			ewarn
			ewarn "Assuming you are going to use ADOdb wrapper for database backend."
			ewarn "You are on you own with this. Unless you know what you are doing,"
			ewarn "you should re-emerge ${PHP_PKG} with at least one of"
			ewarn "${dbflags} USE flags enabled."
	else
		einfo "All PHP checks passed, continue..."
	fi
}

src_install() {
	webapp_src_preinst

	dodoc docs/*
	rm -rf docs/

	cp app/config/database.php.default app/config/database.php
	cp -r . "${D}"/${MY_HTDOCSDIR}

	webapp_serverowned -R "${MY_HTDOCSDIR}"/app/tmp
	for i in app/config/*.php ; do
		webapp_configfile ${MY_HTDOCSDIR}/${i}
	done
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
