# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=81807
# Submitted-By: Douglas Mayle
# Reviewed-By: rl03 2005-12-11

inherit webapp depend.php

MY_PV=${PV//./_}

DESCRIPTION="VideoDB DVD collection management"
HOMEPAGE="http://videodb.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${PN}-${MY_PV}.tgz"

LICENSE="GPL-2 LGPL-2.1"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php_httpd

S=${WORKDIR}/${PN}

pkg_setup() {
	if ! PHPCHECKNODIE="yes" require_php_with_use mysql || \
		! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
			die "Re-install ${PHP_PKG} with mysql and either gd or gd-external USE flags."
	fi

	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	mv "doc/development/videoDB Data Model v2.xml" "doc/development/videoDB_Data_Model_v2.xml"
	dodoc doc/{CHANGES,README,TODO}
	dohtml doc/manual/*
	docinto development
	dodoc doc/development/*
	rm -rf doc/

	cp -R . "${D}"/${MY_HTDOCSDIR}

	local files="cache cache/imdb cache/img cache/smarty config.inc.php images"
	for file in ${files}; do
		webapp_serverowned ${MY_HTDOCSDIR}/${file}
	done

	webapp_configfile ${MY_HTDOCSDIR}/config.inc.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
