# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Parse and format strp and strf time patterns"
HOMEPAGE="http://search.cpan.org/~rickm/${P}/"
SRC_URI="mirror://cpan/authors/id/R/RI/RICKM/${P}.tgz"

LICENSE="|| ( Artistic GPL-2 )"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

SRC_TEST="do"

DEPEND="dev-perl/DateTime
	dev-perl/DateTime-Locale
	dev-perl/DateTime-TimeZone
	dev-perl/Params-Validate"
