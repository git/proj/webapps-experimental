# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

MY_P="${PN/-/_}.pm-${PV}"
S=${WORKDIR}/${MY_P}

DESCRIPTION="Perl interface to the TrustCommerce payment gateway"
HOMEPAGE="http://search.cpan.org/search?query=${PN}&mode=dist"
SRC_URI="mirror://cpan/authors/id/W/WI/WITTEN/${MY_P}.tar.gz"
IUSE=""

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-libs/openssl"
