# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="A module to highlight words or patterns in HTML documents"
SRC_URI="mirror://cpan/authors/id/T/TR/TRIPIE/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~tripie/${P}"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""
