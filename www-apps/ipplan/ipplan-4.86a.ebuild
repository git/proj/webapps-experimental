# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils webapp depend.php depend.apache

DESCRIPTION="A web based multilingual TCP/IP address management software and tracking tool."
HOMEPAGE="http://iptrack.sourceforge.net/"
SRC_URI="mirror://sourceforge/iptrack/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE="examples nls snmp"

WEBAPP_MANUAL_SLOT="yes"
SLOT="0"

RDEPEND="dev-php/adodb"

need_php_httpd
need_apache2

S=${WORKDIR}/${PN}

pkg_setup() {
	local flags="pcre xml zlib"
	local dbflags="mssql mysql postgres oci8 sybase"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} || \
		! PHPCHECKNODIE="yes" require_php_with_any_use ${dbflags} || \
			( use snmp && ! PHPCHECKNODIE="yes" require_php_with_use snmp ) || \
				( use nls && ! PHPCHECKNODIE="yes" require_php_with_use nls ) ; then
					eerror
					eerror "${PN} requires PHP with ${flags} and at least one of ${dbflags} USE flags enabled"
					use nls && eerror "as well as with nls USE flag enabled"
					use snmp && eerror "as well as with snmp USE flag enabled."
					eerror
					die "Re-install ${PHP_PKG} with the above USE flags enabled."
	fi

	webapp_pkg_setup
}

src_unpack() {
	unpack ${A}
	cd "${S}"
	epatch "${FILESDIR}"/${PN}-system-adodb.patch
}


src_install() {
	webapp_src_preinst

	rm -f *WINDOWS*
	dodoc [[:upper:]]*
	if use examples ; then
		docinto contrib
		dodoc contrib/*
	fi

	rm -f [[:upper:]]*
	rm -rf contrib/ adodb/

	cp -R . "${D}"/${MY_HTDOCSDIR}

	keepdir /var/spool/ipplanuploads
	webapp_serverowned /var/spool/ipplanuploads
	chmod 0700 /var/spool/ipplanuploads

	webapp_configfile ${MY_HTDOCSDIR}/config.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
