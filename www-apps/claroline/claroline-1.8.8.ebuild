# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=51662
# Submitted-By: G. Wrobel
# Reviewed-By: wrobel 2005-12-11

inherit webapp depend.php

MY_P=${PN}${PV//./}

DESCRIPTION="Claroline E-learning System"
HOMEPAGE="http://www.claroline.net/"
SRC_URI="http://www.claroline.net/dlarea/${MY_P}.tar.gz"

LICENSE="GPL-2"
IUSE=""
KEYWORDS="~x86"

DEPEND=""
RDEPEND="virtual/httpd-cgi"

need_php_httpd

S=${WORKDIR}/${MY_P}

pkg_setup() {
	webapp_pkg_setup
	require_php_with_use mysql pcre tokenizer zlib
}

src_install() {
	webapp_src_preinst

	cd "${S}"

	## Install documentation
	local DOCS="{CREDITS,INSTALL,README}.txt"
	dodoc ${DOCS}

	## Install application
	cp -R . "${D}"/${MY_HTDOCSDIR}

	## Remove unnecessary documentation from webdir
	for file in ${DOCS} LICENCE.txt ; do
		rm -f "${D}"/${MY_HTDOCSDIR}/${file}
	done

	touch "${D}"/${MY_HTDOCSDIR}/claroline/inc/currentVersion.inc.php

	keepdir ${MY_HTDOCSDIR}/courses
	sed -i -e 's#^\$coursesRepositoryAppend.*#\$coursesRepositoryAppend="courses/";#' "${D}"/${MY_HTDOCSDIR}/claroline/install/do_install.inc.php

	local SFILES="courses claroline claroline/inc/conf claroline/sql \
			claroline/inc/currentVersion.inc.php claroline/lang claroline/admin \
			claroline/inc/conf/*"
	for FILE in ${SFILES} ; do
		webapp_serverowned ${MY_HTDOCSDIR}/${FILE}
	done

	local CFILES="claroline/inc/conf/*"
	for FILE in ${CFILES} ; do
		webapp_configfile ${MY_HTDOCSDIR}/${FILE}
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
