# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

PYTHON_MODNAME="acct_mgr"
TRAC_PV="0.11"
inherit distutils

DESCRIPTION="User account management plugin for Trac"
HOMEPAGE="http://trac-hacks.org/wiki/AccountManagerPlugin"
SRC_URI="http://dev.gentooexperimental.org/~jakub/distfiles/${P}.zip"

LICENSE="BEER-WARE"
KEYWORDS="~x86"
IUSE=""
SLOT="0"

DEPEND="app-arch/unzip
	dev-python/setuptools"
RDEPEND=">=www-apps/trac-${TRAC_PV}_beta"

S="${WORKDIR}/accountmanagerplugin/trunk"

src_test() {
	"${python}" setup.py egg_info
	"${python}" setup.py test || die "tests failed"
}
