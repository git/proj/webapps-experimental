# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=72390
# Submitted-By: Julien Allanos <dju@gentoo.org>
# Updated-By: Jonatan Åkerlind <jonatan.akerlind@telia.com>
# Reviewed-By: wrobel 2006-09-12

inherit webapp

DESCRIPTION="PHP-based iCal file viewer/parser"
HOMEPAGE="http://phpicalendar.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tgz"

KEYWORDS="~amd64 ~x86"
LICENSE="GPL-2"
IUSE=""

RDEPEND="virtual/httpd-cgi
	virtual/httpd-php"

S=${WORKDIR}/${P}/${PN}

src_unpack() {
	unpack ${A}
	cd "${S}"
	find . -type f -name '._*' -print | xargs rm -f
	find . -type f -name '.DS_*' -print | xargs rm -f
	mv config.inc-dist.php config.inc.php
}

src_install() {
	webapp_src_preinst

	local docs="README AUTHORS TIMEZONES"
	dodoc ${docs}
	rm -f ${docs} COPYING

	cp -R . "${D}"/${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/config.inc.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
