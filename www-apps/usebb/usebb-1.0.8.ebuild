# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="Light and Open Source forum package"
HOMEPAGE="http://www.usebb.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

RDEPEND="virtual/httpd-cgi"

need_php_httpd

S="${WORKDIR}/UseBB"

pkg_setup() {
	if ! PHPCHECKNODIE="yes" require_php_with_use pcre session || \
		! PHPCHECKNODIE="yes" require_php_with_any_use mysql mysqli ; then
			die "Re-install ${PHP_PKG} with pcre session and at least one of mysql mysqli USE flags enabled."
	fi

	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	docs="AUTHORS README Changelog.txt docs/INSTALL docs/UPGRADE"
	dodoc ${docs}
	rm -f ${docs} COPYING
	dohtml -r docs/

	mv config.php-dist config.php
	cp -pPR * "${D}/${MY_HTDOCSDIR}"

	webapp_serverowned ${MY_HTDOCSDIR}/config.php
	webapp_configfile ${MY_HTDOCSDIR}/config.php

	webapp_sqlscript mysql install/schemas/mysql.sql
	webapp_sqlscript mysql install/${PN}.sql

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
