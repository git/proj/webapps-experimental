# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp

DESCRIPTION="A Web-based frontend to an ordinary directory structure of your filesystem."
HOMEPAGE="http://webdirectory.benjaminheckmann.de"
SRC_URI="http://www.benjaminheckmann.de/${PN}/${PN}_${PV}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""
S="${WORKDIR}/${PN}"

RDEPEND="virtual/httpd-cgi
	virtual/httpd-php"

src_install() {
	webapp_src_preinst

	dodoc readme.txt

	cp -R . "${D}"/${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/${PN}.ini
	webapp_configfile ${MY_HTDOCSDIR}/auth_demo.ini
	webapp_postinst_txt en readme.txt
	webapp_src_install
}
