# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp

DESCRIPTION="A web interface for darcs"
HOMEPAGE="http://users.auriga.wearlab.de/~alb/darcsweb/"
SRC_URI="http://users.auriga.wearlab.de/~alb/${PN}/files/${PV}/${P}.tar.bz2"

LICENSE="public-domain"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-util/darcs
	dev-lang/python
	virtual/httpd-cgi"

src_install() {
	webapp_src_preinst

	dodoc README LICENSE
	rm -f README LICENSE

	exeinto ${MY_CGIBINDIR}/${PN}
	doexe ${PN}.cgi
	rm -f ${PN}.cgi

	mv config.py.sample config.py

	cp . "${D}"${MY_CGIBINDIR}/${PN}

	webapp_configfile ${MY_CGIBINDIR}/${PN}/config.py
	webapp_postinst_txt en "${FILESDIR}/postinstall-en.txt"

	webapp_src_install
}
