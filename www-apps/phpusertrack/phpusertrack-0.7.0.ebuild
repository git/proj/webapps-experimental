# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

MY_PN="phpUserTrack"
MY_P="${MY_PN}-${PV}"
DESCRIPTION="A site statistic collection and analyzation program written in PHP."
SRC_URI="http://phpusertrack.com/downloads/${MY_P}.tar.gz"
HOMEPAGE="http://www.phpusertrack.com/"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE="geoip minimal tidy"

DEPEND=""
RDEPEND="virtual/httpd-cgi
	dev-php/PEAR-MDB2_Driver_mysqli
	dev-php/PEAR-Structures_Graph
	geoip? ( dev-php/PEAR-Net_GeoIP )
	!minimal? ( dev-php/PEAR-Cache_Lite
		    dev-php/PEAR-MDB2_Driver_mysql )"

need_php_httpd

S="${WORKDIR}/${MY_P}"

pkg_setup() {
	local flags="session" # mysqli already checked for by dev-php/PEAR-MDB2_Driver_mysqli
	use tidy && flags="${flags} tidy"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} || \
		! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
			die "Re-install ${PHP_PKG} with ${flags} and either gd or gd-external"
	fi
	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst

	dodoc ChangeLog
	rm -f ChangeLog INSTALL

	mv conf/conf.php.dist conf/conf.php
	cp -r . "${D}/${MY_HTDOCSDIR}"

	webapp_serverowned "${MY_HTDOCSDIR}"/templates_c
	webapp_serverowned "${MY_HTDOCSDIR}"/cache
	webapp_configfile "${MY_HTDOCSDIR}"/conf/conf.php
	webapp_sqlscript mysql scripts/mysql/create_database.sql

	webapp_src_install
}
