# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="User-friendly and flexible issue tracking system in PHP"
HOMEPAGE="http://dev.mysql.com/downloads/other/eventum/"
# upstream mirrors such way too much to be usable, mirroring manually
SRC_URI="http://dev.gentooexperimental.org/~jakub/distfiles/${P}.tar.gz
	mirror://mysql/Downloads/${PN}/${P}.tar.gz"

LICENSE="GPL-2 PHP LGPL-2.1"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/mysql
	virtual/cron
	virtual/httpd-cgi"

need_php_httpd

pkg_setup() {
	if ! PHPCHECKNODIE="yes" require_php_with_use pcre session mysql || \
		! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
			die "Re-install ${PHP_PKG} with pcre session mysql and either gd or gd-external in USE."
	fi
	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	local docs="ChangeLog FAQ INSTALL README TODO UPGRADE"
	local files="config templates_c locks logs logs/errors.log"

	dodoc ${docs}
	dohtml docs/*

	cp -r . "${D}"${MY_HTDOCSDIR}
	for doc in ${docs} COPYING; do
		rm -r "${D}"/${MY_HTDOCSDIR}/${doc}
	done

	for a in ${files}; do
		webapp_serverowned ${MY_HTDOCSDIR}/${a}
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
