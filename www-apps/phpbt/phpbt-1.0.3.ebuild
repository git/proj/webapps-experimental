# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=113153
# Submitted-By: F. Steinel
# Reviewed-By: rl03 2005-12-10

inherit webapp depend.php

DESCRIPTION="phpBugTracker, A PHP replacement for bugzilla"
HOMEPAGE="http://phpbt.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tgz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE="jpgraph mysql mysqli oci8 postgres"

RDEPEND="dev-php/PEAR-DB
	dev-php/PEAR-Spreadsheet_Excel_Writer
	virtual/httpd-cgi
	jpgraph? ( dev-php5/jpgraph )"

need_php_httpd

pkg_setup() {
	local flags
	for i in mysql mysqli oci8 postgres ; do
		use ${i} && flags="${flags} ${i}"
	done
	require_php_with_use ${flags}
	webapp_pkg_setup
}

src_install () {
	cp config-dist.php config.php

	webapp_src_preinst

	local DOCS="CHANGELOG INSTALL README UPGRADING"
	dodoc ${DOCS}
	dohtml -r docs/html/*
	rm -rf ${DOCS} docs/

	cp -R . "${D}"/${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/config.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}
