# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Reviewed-By: bathym 2006-05-12

inherit webapp depend.php depend.apache

#MY_PV=${PV/_beta/beta.}

DESCRIPTION="TUTOS is a tool to manage the organizational needs of small groups, teams, departments"
HOMEPAGE="http://www.tutos.org"
SRC_URI="mirror://sourceforge/${PN}/TUTOS-php-${PV}.tar.bz2"
LICENSE="GPL-2"
KEYWORDS="~x86"

DB_BACKENDS="firebird interbase mysql oci8 postgres"
IUSE="${DB_BACKENDS} ldap"

DEPEND=""
RDEPEND=""

need_php_httpd
need_apache

S=${WORKDIR}/${PN}

pkg_setup() {
	local dbflags
	for db in ${DB_BACKENDS} ; do
		use ${db} && dbflags="${dbflags} ${db}"
	done
	if use ldap ; then
		local ldapmsg="and also ldap"
		local flags="ldap"
	fi

	if [[ -z ${dbflags} ]] ; then
		if ! PHPCHECKNODIE="yes" require_php_with_any_use ${DB_BACKENDS} || \
			( use ldap && ! PHPCHECKNODIE="yes" require_php_with_use ldap ) ; then
				die "Re-install ${PHP_PKG} with at least one of ${DB_BACKENDS} ${ldapmsg} USE flags enabled."
		fi
	else
		require_php_with_use ${dbflags} ${flags}
	fi

	webapp_pkg_setup
}

src_unpack() {
	unpack ${A}
	cd "${S}"
	cp php/config_default.pinc php/config.php
	rm -f tutos.{spec*,lsm}
}

src_install () {
	webapp_src_preinst
	local docs="ChangeLog INSTALL README* ToDo"
	dodoc ${docs}
	rm -f ${docs}

	cp -R . "${D}"/${MY_HTDOCSDIR}

	keepdir ${MY_HTDOCSDIR}/repository
	webapp_serverowned ${MY_HTDOCSDIR}/repository
	webapp_configfile ${MY_HTDOCSDIR}/php/config.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_hook_script "${FILESDIR}"/reconfig

	webapp_src_install
}
