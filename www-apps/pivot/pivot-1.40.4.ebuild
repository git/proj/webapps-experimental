# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=86694
# Submitted-By: didbaba
# Reviewed-By: rl03 2005-12-11

inherit versionator webapp depend.php

MY_PV=$(delete_all_version_separators ${PV} )
DESCRIPTION="Pivot is a web-based tool to help you maintain dynamic sites, like weblogs or online journals"
HOMEPAGE="http://www.pivotlog.net"
SRC_URI="mirror://sourceforge/pivot-weblog/${PN}_${MY_PV}_full.zip"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND="virtual/httpd-cgi"

need_php_httpd

S=${WORKDIR}

src_install () {
	webapp_src_preinst
	dodoc README.txt example.htaccess
	rm -f README.txt example.htaccess

	cp -R . "${D}"/${MY_HTDOCSDIR}

	local files="archives images pivot/db pivot/templates
		pivot/pv_cfg_settings.php pivot/pv_cfg_weblogs.php"

	webapp_serverowned "${MY_HTDOCSDIR}"
	for x in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}/${x}"
	done
	cd pivot/db
	for y in $(find . -type f -print); do
		webapp_serverowned "${MY_HTDOCSDIR}/pivot/db/${y}"
	done
	cd ../templates
	for z in $(find . -type f -print); do
		webapp_serverowned "${MY_HTDOCSDIR}/pivot/templates/${z}"
	done

	webapp_configfile "${MY_HTDOCSDIR}/pivot/pv_cfg_settings.php"
	webapp_configfile "${MY_HTDOCSDIR}/pivot/pv_cfg_weblogs.php"
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
