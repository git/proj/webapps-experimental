# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Parser for plain-text configuration files"
SRC_URI="mirror://cpan/authors/id/C/CO/CORLISS/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~corliss/${P}"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""
SLOT="0"

SRC_TEST="do"

DEPEND="virtual/perl-File-Spec
	dev-perl/module-build"
