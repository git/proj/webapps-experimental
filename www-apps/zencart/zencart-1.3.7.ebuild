# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=84239
# Submitted-By: Marco Nierlich
# Reviewed-By: rl03 2005-12-16

inherit eutils webapp depend.php

MY_PN="zen-cart"
PATCHREV="07052007"
MY_P="${MY_PN}-v${PV}-full-patched-${PATCHREV}"

DESCRIPTION="Online e-commerce shopping solution"
HOMEPAGE="http://www.zencart.com"
SRC_URI="mirror://sourceforge/${PN}/${MY_P}.zip"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND="virtual/httpd-cgi"

need_php_httpd

S=${WORKDIR}/${MY_P}

src_unpack() {
	unpack ${A}
	cd "${S}"
	mv admin/includes/dist-configure.php admin/includes/configure.php
	mv includes/dist-configure.php includes/configure.php
}

pkg_setup() {
	local optional="curl ftp gd ssl xml zlib apache2 sqlite"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${optional} || \
		! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
		    ewarn
		    ewarn "NOTE: The above use flags are not enabled for your PHP install"
		    ewarn "but are strongly recommended to make use of full features of ${PN}:"
		    ewarn "Consider re-emerging ${PHP_PKG} with those USE flags enabled."
		    ewarn
		    ebeep
		    epause 5
	fi
	require_php_with_use mysql session

	webapp_pkg_setup

	ewarn
	ewarn "phpBB is no longer supported by this ebuild due to security issues."
	ewarn "If you really want it, unmask and emerge www-apps/phpBB manually."
	ewarn
	epause 3
}

src_install() {
	webapp_src_preinst

	dodoc docs/*.txt
	dohtml docs/*
	rm -rf {install,license}.txt

	cp -R . "${D}"/${MY_HTDOCSDIR}

	local files="admin/includes/configure.php includes/configure.php cache
		images includes/languages/english/html_includes media pub admin/backups
		admin/images/graphs"
	for file in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}/${file}"
	done
	cd images
	for a in $(find . -type d); do
		webapp_serverowned "${MY_HTDOCSDIR}/images/${a}"
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_configfile ${MY_HTDOCSDIR}/includes/configure.php
	webapp_configfile ${MY_HTDOCSDIR}/admin/includes/configure.php
	webapp_src_install
}
