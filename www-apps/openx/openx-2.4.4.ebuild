# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="Advanced web-based ad management"
HOMEPAGE="http://www.openx.org/"
SRC_URI="http://www.openx.org/downloads/${P}.zip"

LICENSE="GPL-2 LGPL-2.1"
KEYWORDS="~amd64 ~x86"
IUSE="gd"

# TODO: Wipe all the bundled PEAR stuff and depend on appropriate
# dev-php/PEAR-* ebuilds instead.
DEPEND="app-arch/unzip"
RDEPEND="virtual/cron"
need_php_httpd

pkg_setup() {
	webapp_pkg_setup

	local flags="crypt ctype pcre session unicode xml zlib"
	local dbflags="mysql mysqli postgres"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} \
	    || ! PHPCHECKNODIE="yes" require_php_with_any_use mysql mysqli postgres \
		|| use gd && ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
		    eerror
		    eerror "${PHP_PKG} needs to be re-installed with all of the following"
		    eerror "USE flags enabled:"
		    eerror
		    eerror "${flags}"
		    eerror
		    eerror "as well as at least one of ${dbflags} USE flags enabled"
		    use gd && eerror "and either gd or gd-external USE flag enabled."
		    die "Re-install ${PHP_PKG}"
	fi
}

src_install() {
	webapp_src_preinst

	local docs="README.txt RELEASE_NOTES.txt UPGRADE.txt docs/KNOWN_ISSUES.txt"
	dodoc ${docs}
	rm -rf ${docs} INSTALL.txt docs/

	cp -r . "${D}${MY_HTDOCSDIR}"

	touch "${D}${MY_HTDOCSDIR}"/config.inc.php
	webapp_configfile "${MY_HTDOCSDIR}"/config.inc.php
	webapp_serverowned "${MY_HTDOCSDIR}"/config.inc.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en-2.4.txt

	for dir in var var/cache var/plugins var/plugins/cache var/plugins/config var/templates_compiled www/images ; do
		webapp_serverowned "${MY_HTDOCSDIR}"/${dir}
	done

	webapp_src_install
}

pkg_postinst() {
	if ! use gd ; then
		ewarn "If you would like to have support for displaying graphs in the ${PN}"
		ewarn "statistics pages, you will also need to make sure that PHP is installed"
		ewarn "with support for GD."
	fi
	webapp_pkg_postinst
}
