# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Reviewed-By: bathym 2006-4-12

inherit webapp depend.php

DESCRIPTION="Open Source web content management system optimized for fast and easy setup."
HOMEPAGE="http://www.phpwcms.de/ http://www.phpwcms-docu.de/"
# SRC_URI="mirror://sourceforge/${PN}/${P}.tgz"
SRC_URI="http://kent.dl.sourceforge.net/sourceforge/${PN}/${P/-/_}.tgz"

LICENSE="GPL-2 LGPL-2.1 public-domain htmlArea"
KEYWORDS="~x86"
IUSE="gs imagemagick"

RDEPEND="virtual/httpd-cgi
	imagemagick? ( media-gfx/imagemagick )
	gs? ( virtual/ghostscript )"

need_php_httpd

S=${WORKDIR}/${P/-/_}

move_junk() {
	# Move files that people don't know how to name
	einfo "Moving files with spaces in names"
	cd ./include/inc_module/mod_graphical_text || die
	mv ./update\ 1.0\ to\ 1.1.txt ./update_1.0_to_1.1.txt || die
	mv ./update\ 1.1\ to\ 1.3.txt ./update_1.1_to_1.3.txt || die
	mv ./replacement\ tags.txt ./repacement_tags.txt || die
	mv ./update\ 1.3\ to\ 1.4.txt ./update_1.3_to_1.4.txt || die
	mv ./update\ 1.4\ to\ 1.5.txt ./update_1.4_to_1.5.txt || die
	mv ./update\ 1.5\ to\ 2.0.txt ./update_1.5_to_2.0.txt || die
	einfo "Done!"
}

pkg_setup() {
	local diemsg flags="mysql"
	if use gd ; then
		flags="${flags} truetype"
		if ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
			diemsg="and either gd or gd-external"
		fi
	fi
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} || \
		( use gd && ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ) ; then
			die "Re-install ${PHP_PKG} with ${flags} ${diemsg} in USE."
	fi
	webapp_pkg_setup
}

src_unpack() {
	unpack ${A}
	cd "${S}"
	move_junk
}

src_install() {
	webapp_src_preinst

	local docs="LICENSE.txt changelog.txt"
	dodoc ${docs}
	rm -f ${docs} GPL.*
	mv _.htaccess .htaccess

	cp -r . "${D}"/${MY_HTDOCSDIR}

	local files="content content/ads content/form content/gt content/images content/pages content/rss content/tmp
		filearchive filearchive/can_be_deleted
		setup/setup.conf.inc.php
		template template/inc_css/frontend.css template/inc_default/startup.php
		upload"

	for i in ${files}; do
		webapp_serverowned ${MY_HTDOCSDIR}/${i}
	done

	cd "${S}"
	for cfgfile in config/phpwcms/* ; do
		webapp_configfile ${MY_HTDOCSDIR}/${cfgfile}
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_hook_script "${FILESDIR}"/reconfig

	webapp_src_install
}
