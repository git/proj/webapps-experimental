# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=88428
# Submitted-By: Christoph Bussenius <spam@der-pepe.de>
# Reviewed-By: wrobel 2005-12-14

inherit eutils toolchain-funcs autotools

DESCRIPTION="Make HTML archives out of mailboxes"
HOMEPAGE="http://www.hypermail-project.org/"
SRC_URI="http://www.hypermail-project.org/$P.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="gdbm"

DEPEND="gdbm? ( sys-libs/gdbm )"

src_unpack() {
	unpack ${A}
	cd "${S}"

	sed -i -e '/^stripprog=/s:${STRIPPROG-strip}::' install-sh || die "sed failed"
	# This patch makes DESTDIR work for make install
	epatch "${FILESDIR}"/destdir.patch
	eautoreconf
}

src_compile() {
	# fix cross-compile
	tc-export CC

	econf \
		--with-htmldir=/usr/share/doc/${PF}/html \
		$(use_with gdbm)
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
	dodoc Changelog KNOWN_BUGS README TODO UPGRADE || die 'dodoc failed'
}
