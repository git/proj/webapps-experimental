# Jakub Moc <jakub@gentoo.org> (19 Feb 2008)
# requires masked trac version
=www-apps/trac-accountmanager-0.2

# Jakub Moc <jakub@gentoo.org> (09 Sept 2007)
# This stuff is masked until I have time to get 
# these ebuilds updated and fixed.
games-rpg/lotgd

# Kevin G Fourie <kevin@4e.co.za> (07 Sep 2007)
# masked pending testing 
>=www-apps/knowledgetree-3.5
