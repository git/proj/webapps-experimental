# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="extension for reading and resolving symbolic links"
SRC_URI="http://search.cpan.org/CPAN/authors/id/R/RM/RMBARKER/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~rmbarker/${P}/"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""

SRC_TEST="do"

DEPEND="dev-perl/module-build
	virtual/perl-File-Spec"
