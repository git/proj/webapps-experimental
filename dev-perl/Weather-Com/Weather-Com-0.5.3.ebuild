# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Fetching weather information from weather.com."
SRC_URI="mirror://cpan/authors/id/S/SC/SCHNUECK/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~schnueck/${P}"
IUSE=""

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"

DEPEND="dev-perl/libwww-perl
	>=dev-perl/Time-Format-1.0
	dev-perl/URI
	dev-perl/XML-Simple
	virtual/perl-Storable
	virtual/perl-Time-Local"

SRC_TEST="do"
