# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="PHP-powered, flexible blogging/CMS application."
HOMEPAGE="http://www.s9y.org/"
SRC_URI="mirror://sourceforge/php-blog/${P}.tar.gz"

LICENSE="BSD"
KEYWORDS="~amd64 ~x86"
IUSE="imagemagick"

DEPEND=""
RDEPEND="virtual/httpd-cgi
	imagemagick? ( media-gfx/imagemagick )"

need_php_httpd

S="${WORKDIR}/${PN}"

pkg_setup() {
	 webapp_pkg_setup

	if  ! PHPCHECKNODIE="yes" require_php_with_any_use mysql mysqli postgres sqlite \
		|| ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external \
			|| ! PHPCHECKNODIE="yes" require_php_with_use iconv pcre unicode zlib ; then
			eerror
			eerror "${PHP_PKG} needs to be reinstalled at least one of mysql, mysqli,"
			eerror "postgres or sqlite USE flags enabled for a database backend."
			eerror "It must also have either gd or gd-external USE flag enabled."
			eerror "It must also have all of iconv pcre unicode zlib USE flags enabled."
			die "Reinstall ${PHP_PKG} with the above-stated USE flags."
	fi
}

src_install() {
	webapp_src_preinst

	rm docs/INSTALL*
	local docs="docs/*"
	dodoc ${docs}
	rm -rf docs/

	einfo "Installing application files."
	cp -r . "${D}"${MY_HTDOCSDIR}
	touch "${D}"${MY_HTDOCSDIR}/.htaccess
	touch "${D}"${MY_HTDOCSDIR}/serendipity_config_local.inc.php

	keepdir ${MY_HTDOCSDIR}/archives/
	webapp_configfile ${MY_HTDOCSDIR}/serendipity_config_local.inc.php
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	# archives, templates_c and uploads need to be writable to PHP
	# .htaccess amd serendipity_config_local.inc.php need to be too
	# plugins and templates need to be writable for spartacus support.
	local SERVEROWNED="serendipity_config_local.inc.php .htaccess archives plugins templates templates_c uploads"
	for SFILES in ${SERVEROWNED}; do
		webapp_serverowned ${MY_HTDOCSDIR}/${SFILES}
	done

	webapp_src_install
}
