# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Simple calculations with RGB colors."
SRC_URI="mirror://cpan/authors/id/C/CF/CFAERBER/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~cfaerber/${P}"

SLOT="0"
LICENSE="|| ( GPL-1 GPL-2 GPL-3 Artistic )"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=dev-perl/Params-Validate-0.75
	>=dev-perl/Graphics-ColorNames-0.32
	>=dev-perl/Graphics-ColorNames-WWW-0.01"

SRC_TEST="do"
