# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="WWW color names and equivalent RGB values"
SRC_URI="mirror://cpan//authors/id/C/CF/CFAERBER/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~cfaerber/${P}"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=dev-perl/Graphics-ColorNames-0.32"

SRC_TEST="do"
