# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

MY_PN="ackerTodo"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="ackerTodo is a light-weight web based todo list manager"
HOMEPAGE="http://ackertodo.sourceforge.net/site2/index.html"
SRC_URI="mirror://sourceforge/${PN}/${MY_PN}-${PV}.tar.bz2"
LICENSE="GPL-2"

KEYWORDS="~x86"
IUSE="email-notification"

DEPEND=""
RDEPEND="email-notification? ( virtual/cron )
	virtual/httpd-cgi"

need_php_httpd

S=${WORKDIR}/${MY_P}

pkg_setup() {
	webapp_pkg_setup
	if use email-notification ; then
		require_php_with_use cli mysql
	else
		require_php_with_use mysql
	fi
}

src_install() {
	webapp_src_preinst

	local docs="ChangeLog CREDITS README TODO UPGRADE"

	dodoc ${docs}
	for doc in ${docs}; do
		rm -f ${doc}
	done

	cp -r src/* "${D}"${MY_HTDOCSDIR}

	webapp_configfile ${MY_HTDOCSDIR}/config/config.inc.php
	webapp_sqlscript mysql "${D}"${MY_HTDOCSDIR}/setup/ackertodo.sql
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}

pkg_postinst() {
	if use email-notification ; then
		elog "See Email notification section of README in /usr/share/doc/${PF}"
		elog "for instructions on using the email module."
	fi
	webapp_pkg_postinst
}
