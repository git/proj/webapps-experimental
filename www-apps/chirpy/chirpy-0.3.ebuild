# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.apache

DESCRIPTION="An online quote management system."
HOMEPAGE="http://chirpy.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tgz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=dev-lang/perl-5.8
	dev-perl/Data-Dumper
	dev-perl/DBI
	dev-perl/DBD-mysql
	dev-perl/HTML-Template
	dev-perl/libwww-perl
	dev-perl/URI
	virtual/perl-CGI
	virtual/perl-Digest-MD5
	virtual/perl-Storable"
RDEPEND="${DEPEND}
	>=virtual/mysql-4.1"

need_apache

S="${WORKDIR}"

src_install() {
	webapp_src_preinst
	dodir ${MY_HOSTROOTDIR}/${PF} ${MY_CGIBINDIR}/${PN}

	dodoc *.txt
	cp -R .htaccess res "${D}"/${MY_HTDOCSDIR}
	cp index.cgi "${D}"/${MY_CGIBINDIR}/${PN}
	cp -R src util "${FILESDIR}"/chirpy.ini "${D}"/${MY_HOSTROOTDIR}/${PF}

	webapp_configfile ${MY_HOSTROOTDIR}/${PF}/chirpy.ini
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_hook_script "${FILESDIR}"/reconfig
	webapp_src_install
}
