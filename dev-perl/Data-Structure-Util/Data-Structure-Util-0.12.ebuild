# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="Change nature of data within a structure"
SRC_URI="http://search.cpan.org/CPAN/authors/id/F/FO/FOTANGO/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~pdenis/${P}"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~x86"
IUSE=""

DEPEND="dev-perl/Test-Pod
	dev-perl/Clone
	dev-perl/module-build"

RDEPEND="virtual/perl-Digest-MD5"
