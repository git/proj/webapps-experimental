# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="A rrdtool harvester and a lightweight replacement for cacti."
HOMEPAGE="http://serverstats.berlios.de/"
#SRC_URI="http://download.berlios.de/${PN}/${P}.tar.bz2"
SRC_URI="mirror://berlios/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE=""

RDEPEND=">=net-analyzer/rrdtool-1.2
	virtual/cron
	virtual/httpd-cgi"

need_php5_httpd

src_unpack() {
	unpack "${A}"
	cd "${S}"
	mv config.sample/ config
}

pkg_setup() {
	webapp_pkg_setup

	if ! PHPCHECKNODIE="yes" require_php_with_use cli \
		|| ! PHPCHECKNODIE="yes" require_php_with_any_use apache2 cgi ; then
			die "Re-install ${PHP_PKG} with cli and at least one of apache2 cgi USE flags."
	fi
}

src_install() {
	webapp_src_preinst

	einfo "Installing docs"
	dodoc doc/*.txt
	docinto examples
	newdoc sources/traffic/iptables.sample iptables-rules.sample
	newdoc sources/traffic/traffic.sh traffic.sh.sample
	rm sources/traffic/iptables.sample
	rm sources/traffic/traffic.sh
	rm -rf doc/
	rm -f INSTALL LICENSE sources/external/cacti/{LICENSE,README}

	einfo "Installing main files"
	cp -pPR . "${D}/${MY_HTDOCSDIR}"

	webapp_serverowned "${MY_HTDOCSDIR}"/graph

	cd "${D}/${MY_HTDOCSDIR}"
	local CONFFILE
	for CONFFILE in config/* ; do
		webapp_configfile "${MY_HTDOCSDIR}/${CONFFILE}"
	done

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_src_install
}
