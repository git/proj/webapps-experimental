# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit webapp depend.php

DESCRIPTION="A PHP-based calendar application"
HOMEPAGE="http://webcalendar.sourceforge.net"

MY_PN=WebCalendar
MY_P=${MY_PN}-${PV}

SRC_URI="mirror://sourceforge/${PN}/${MY_P}.tar.gz"
LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE="gradient ldap reminders"

RDEPEND="virtual/httpd-php
	virtual/httpd-cgi
	reminders? ( virtual/cron )"

S="${WORKDIR}/${MY_P}"

pkg_setup () {
	dbflags="db2 firebird interbase mssql mysql mysqli oci8 odbc postgres sqlite"
	if ! PHPCHECKNODIE="yes" require_php_with_any_use ${dbflags} || \
	    ! PHPCHECKNODIE="yes" require_php_with_use session || \
		( use gradient && ! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ) || \
		    ( use ldap && ! PHPCHECKNODIE="yes" require_php_with_use ldap ) ; then
			eerror
			eerror "You need PHP with session and at least one of the following USE flags enabled:"
			eerror "${dbflags}"
			use gradient && eerror "and with either gd or gd-external USE flag enabled"
			use ldap && eerror "and also with ldap USE flag enabled"
			eerror
			die "Re-install ${PHP_PKG} with the above USE flags enabled."
	fi

	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	local docs="AUTHORS NEWS docs/README"
	dodoc ${docs}
	dohtml README.html
	dohtml -r docs/
	rm -f ${docs} ${GPL,UPGRADING}.html tools/summary.txt tools/upgrade*.pl

	use reminders && sed -i -e 's:/local::' tools/send_reminders.php

	cp -R * ${D}/${MY_HTDOCSDIR}

	webapp_serverowned ${MY_HTDOCSDIR}/includes
	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	for i in includes/user*.php ; do
		webapp_serverowned ${MY_HTDOCSDIR}/includes/${i}
		webapp_configfile ${MY_HTDOCSDIR}/includes/${i}
	done

	webapp_src_install
}
