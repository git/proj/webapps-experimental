# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils versionator

MY_PV=$(get_version_component_range 1-2)
MY_P="${PN}_${MY_PV}"
DEBPATCH="${PN}_${PV/_p/-}.diff"

DESCRIPTION="A small and simple personal WikiWikiWeb"
# HOMEPAGE="http://didiwiki.org/"
HOMEPAGE="http://packages.debian.org/unstable/web/didiwiki"
SRC_URI="mirror://debian/pool/main/d/didiwiki/${MY_P}.orig.tar.gz
	mirror://debian/pool/main/d/didiwiki/${DEBPATCH}.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

S=${WORKDIR}/${MY_P/_/-}

src_unpack() {
	unpack ${A}
	epatch ${DEBPATCH}
}

src_compile() {
	econf --prefix=/usr || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	dobin src/didiwiki
	dodoc AUTHORS ChangeLog README TODO
	doman debian/${PN}.1

	newinitd "${FILESDIR}"/${PN}.rc didiwiki
	newconfd "${FILESDIR}"/${PN}.conf didiwiki
}

pkg_postinst() {
	if has_version "<${CATEGORY}/${P}" ; then
		elog "delete ~/.didiwiki/WikiHelp to get the latest Help file."
	fi
}
