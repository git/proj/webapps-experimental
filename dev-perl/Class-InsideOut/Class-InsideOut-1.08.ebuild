# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

DESCRIPTION="A safe, simple inside-out object construction kit."
SRC_URI="mirror://cpan/authors/id/D/DA/DAGOLDEN/${P}.tar.gz"
HOMEPAGE="http://search.cpan.org/~dagolden/${P}"
IUSE=""

SLOT="0"
LICENSE="Apache-2.0"
KEYWORDS="~x86"

DEPEND=">=virtual/perl-Scalar-List-Utils-1.09
	dev-perl/Class-ISA"

SRC_TEST="do"
