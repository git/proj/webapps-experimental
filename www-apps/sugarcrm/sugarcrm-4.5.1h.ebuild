# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Source: http://bugs.gentoo.org/show_bug.cgi?id=103295
# Submitted-By: SteveB
# Reviewed-By: wrobel 2005-12-19

inherit webapp depend.php

MY_PN="SugarOS"
DESCRIPTION="A complete CRM and groupware system for businesses of all sizes."
HOMEPAGE="http://www.sugarcrm.com"
SRC_URI="mirror://sourceforge/${PN}/${MY_PN}-${PV}.zip"

LICENSE="MPL-1.1 SPL-1.1.3"
KEYWORDS="~x86"
S=${WORKDIR}/${MY_PN}-Full-${PV}

IUSE="curl imap json zlib"

DEPEND="app-arch/unzip"
RDEPEND="dev-php/PEAR-DB
	    dev-php/PEAR-Cache_Lite
	    dev-php/PEAR-Mail_Mime
	    virtual/httpd-cgi"

need_php5_httpd

pkg_setup() {
	local flags="mysql ssl unicode xml"
	for f in curl imap json zlib ; do
		use ${f} && flags="${flags} ${f}"
	done
	if ! PHPCHECKNODIE="yes" require_php_with_use ${flags} ; then
		eerror "${PHP_PKG} needs to be reinstalled with all of ${flags} USE flags enabled"
		die "Reinstall ${PHP_PKG} with the above-stated USE flags."
	fi

	webapp_pkg_setup
}

src_install () {
	webapp_src_preinst

	cd "${S}"

	dodoc README.txt
	rm -f {INSTALLATION,LICENSE,PATCH,README,UPGRADE}.txt

	einfo "Installing main files"
	cp -R . "${D}"/${MY_HTDOCSDIR}

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt

	webapp_configfile ${MY_HTDOCSDIR}/config.php
	webapp_configfile ${MY_HTDOCSDIR}/.htaccess

	for foo in cache custom data modules ; do
		webapp_serverowned -R ${MY_HTDOCSDIR}/${foo}
	done
	webapp_serverowned ${MY_HTDOCSDIR}/config.php

	webapp_src_install
}
