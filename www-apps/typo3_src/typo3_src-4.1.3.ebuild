# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils webapp depend.php

DESCRIPTION="TYPO3 is a free Open Source CMS. This is the source package."
HOMEPAGE="http://www.typo3.com/  http://typo3.org/"
SRC_URI="mirror://sourceforge/typo3/${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86"
IUSE="imagemagick mysql"

RDEPEND="~www-apps/typo3_dummy-${PV}
	virtual/httpd-cgi
	imagemagick? ( media-gfx/imagemagick )"

need_php5_httpd

pkg_setup() {
	local optional="truetype zlib"
	if ! PHPCHECKNODIE="yes" require_php_with_use ${optional} || \
		! PHPCHECKNODIE="yes" require_php_with_any_use gd gd-external ; then
		    ewarn
		    ewarn "NOTE: The above use flags are not enabled for your PHP install"
		    ewarn "but are strongly recommended to make use of full features of ${PN}."
		    ewarn "Consider re-emerging ${PHP_PKG} with those USE flags enabled."
		    ewarn
		    ebeep
		    epause 3
	fi

	# check for mysql support first, this is the preferred and primary DB backend
	if use mysql ; then
		require_php_with_use mysql
	else
		# check for at least one of DB backends supported by the bundled ADOdb
		local adodb="db2 firebird interbase mssql mysql mysqli oci8 odbc pdo postgres sapdb sqlite sybase"
		ewarn
		ewarn "MySQL is the recommended DB backend for ${PN} but you do not have USE=mysql enabled."
		ewarn "Support for other DB backends is provided via ADOdb abstraction and includes:"
		ewarn "${adodb}"
		ewarn
		ebeep
		epause 3
		require_php_with_any_use ${adodb}
	fi

	webapp_pkg_setup
}

src_install() {
	webapp_src_preinst

	rm -f {GPL,LICENSE}.txt
	dodoc *.txt ChangeLog
	rm -f *.txt ChangeLog

	dodir ${MY_HOSTROOTDIR}/${PF}
	cp -R . "${D}"/${MY_HOSTROOTDIR}/${PF}

	webapp_postinst_txt en "${FILESDIR}"/postinstall-en-4.txt
	webapp_src_install
}

pkg_postinst() {
	elog
	elog "Some sort of PHP cache is highly recommended for ${PN}."
	elog "If you have not installed one yet, emerge one of the following ebuilds:"
	elog "		dev-php5/eaccelerator"
	elog "		dev-php5/pecl-apc"
	elog "		dev-php5/ZendOptimizer"
	elog

	webapp_pkg_postinst
}
